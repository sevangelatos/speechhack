#!/usr/bin/env python
"""
Sound snippet recorder, with sound detection
"""
import array
import collections
import math
import Queue
import struct
import threading
import pyaudio

__all__ = ['Recorder', 'rmsdb', 'wav_header']

# Recorder states
ST_PAUSE = 1
ST_REC = 2
ST_WAIT = 3
ST_FIN = 4


class RecSettings(object):

    """
    Settings for the audio recorder
    """

    def __init__(self):
        self.stop_silence = 2
        self.quality = (16000, 1)  # 16k mono
        self.max_rec_duration = 9.5
        self.min_sound_duration = 0.2
        self.silence_threshold = -35


class Recorder(object):

    """
    Record audio asynchronously, with automatic sound and silence detection
    """

    def __init__(self, settings=RecSettings()):
        self.audio_queue = Queue.Queue()
        self.cmd_queue = Queue.Queue(maxsize=1)
        self.settings = settings
        self.worker = threading.Thread(target=self.__work)
        self.worker.daemon = True
        self.worker.start()

    def __work(self):
        """
        Worker thread entry point
        """
        chunk_samples = 512
        rate, channels = self.settings.quality
        max_chunks = int(
            (self.settings.max_rec_duration * rate) / chunk_samples)
        max_samples = max_chunks * chunk_samples
        header = wav_header(
            samples=max_samples, samplerate=rate, channels=channels)

        try:
            sample_format = pyaudio.paInt16
            chunk = chunk_samples * channels
            audio = pyaudio.PyAudio()
            # start Recording
            stream = audio.open(format=sample_format, channels=channels,
                                rate=rate, input=True,
                                frames_per_buffer=chunk)
            self.recording_loop(stream, header, chunk_samples, max_chunks)
        except Exception as ex:
            print ex
            # Terminate stream
            self.audio_queue.put("")

         # stop Recording
        stream.stop_stream()
        stream.close()
        audio.terminate()

    def recording_loop(self, instream, header, chunk_samples, max_chunks):
        "Loop for monitoring audio and pushing in the audio_queue"
        sent = 0
        preroll_sec = 0.2 + self.settings.min_sound_duration
        rate, channels = self.settings.quality
        sec2chunks = lambda s: int(round(s * rate / float(chunk_samples)))
        stop_chunks = sec2chunks(self.settings.stop_silence)
        min_snd_chunks = sec2chunks(self.settings.min_sound_duration)
        amplitudes = collections.deque(maxlen=max(stop_chunks, min_snd_chunks))
        preroll = collections.deque(maxlen=sec2chunks(preroll_sec))
        is_silence = lambda db: db < self.settings.silence_threshold
        is_sound = lambda db: db >= self.settings.silence_threshold
        state = ST_PAUSE

        while state != ST_FIN:
            chunk = instream.read(chunk_samples * channels)
            old_state = state

            # de-queue a command
            cmd = self.__cmd()
            if cmd:
                state = cmd

            if state != ST_PAUSE:
                amplitudes.append(rmsdb(chunk))

            if state == ST_WAIT and trailing_chunks(amplitudes, is_sound) >= min_snd_chunks:
                state = ST_REC

            if state == ST_WAIT:
                preroll.append(chunk)

            if state == ST_REC and old_state != ST_REC:
                self.audio_queue.put(header)
                for chk in preroll:
                    self.audio_queue.put(chk)
                    sent += 1
                preroll.clear()

            if state == ST_REC:
                self.audio_queue.put(chunk)
                sent += 1
                if sent >= max_chunks or trailing_chunks(amplitudes, is_silence) >= stop_chunks:
                    old_state = ST_REC
                    state = ST_PAUSE

            if old_state == ST_REC and state != ST_REC:
                if sent:
                    self.audio_queue.put("")  # Terminate stream
                sent = 0
                preroll.clear()
                amplitudes.clear()

            if cmd:
                self.cmd_queue.task_done()

    def __change_state(self, msg):
        self.cmd_queue.put(msg)
        self.cmd_queue.join()

    def pause(self):
        "Pause recording"
        self.__change_state(ST_PAUSE)

    def start(self):
        "Start recording"
        self.__change_state(ST_REC)

    def close(self):
        "Close audio device and release used resources"
        self.__change_state(ST_FIN)
        self.worker.join()

    def start_on_sound(self):
        "Wait for sound to start recording"
        self.__change_state(ST_WAIT)

    def audio_frames(self):
        """
        Returns a generator that will iterate over the frames
        of the recorded audio as it is being recorded
        """
        frame = True
        while frame:
            frame = self.audio_queue.get()
            if frame:
                yield frame
            self.audio_queue.task_done()

    def __cmd(self):
        try:
            return self.cmd_queue.get_nowait()
        except Queue.Empty:
            return None


def trailing_chunks(amplitudes, cond):
    "Count the number of last chunks which satisfy the condition"
    count = len(amplitudes)
    for i in xrange(count - 1, 0, -1):
        if not cond(amplitudes[i]):
            return count - i - 1
    return len(amplitudes)


def rmsdb(pcm):
    """
    Calculate the RMS amplitude of a PCM signal

    The signal must be little endian, 16bit PCM data
    """
    data = array.array("h", pcm)
    count = len(data)
    mean = sum(data) / count
    norm = 1.0 / ((2**15) - 1)
    scale = norm * norm
    mean_square = sum([(s - mean) * (s - mean) * scale for s in data]) / count
    return 10 * math.log(mean_square, 10)


def wav_header(samples, samplerate=16000, channels=1, bytes_per_sample=2):
    """
    Produce the entire PCM wav header preceeding the PCM data.

    The WAV header is the preamble specifying the format of the PCM stream.
    The samples parameter is equal to duration_seconds/samplerate.

    >>> wav_header(76800)
    'RIFF$X\\x02\\x00WAVEfmt \\x10\\x00\\x00\\x00\\x01\\x00\\x01\\x00\\x80>\\x00\\x00\\x00}\\x00\\x00\\x02\\x00\\x10\\x00data\\x00X\\x02\\x00'
    """
    data_bytes = samples * bytes_per_sample * channels
    stream_format = 1  # PCM
    header_len = 44
    riff_bytes = data_bytes + header_len - 8
    byte_rate = samplerate * channels * bytes_per_sample
    uint32le = lambda x: struct.pack("<I", x)
    uint16le = lambda x: struct.pack("<H", x)
    header = ['RIFF', uint32le(riff_bytes), 'WAVE',
              'fmt \x10\x00\x00\x00', uint16le(
                  stream_format), uint16le(channels),
              uint32le(samplerate), uint32le(byte_rate),
              uint16le(bytes_per_sample * channels),
              uint16le(bytes_per_sample * 8),
              'data', uint32le(data_bytes)]
    return ''.join(header)


def testme():
    """
    Record one sound and save it on disk.
    """
    rec = Recorder()
    rec.start_on_sound()

    with open("/tmp/hello.wav", "wb") as out:
        for frame in rec.audio_frames():
            out.write(frame)

    rec.close()

if __name__ == "__main__":
    testme()
