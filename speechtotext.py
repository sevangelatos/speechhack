#!/usr/bin/env python

import requests
import uuid
import time

app_id = "36dfed95-2af2-4629-93f6-313004bf313e"


class SpeechToText(object):

    """
    Speech to text instance that maintains and renews as needed a session.
    """

    def __init__(self, secret, device_id):
        self.secret = secret
        self.device_id = device_id
        self.session_token = None
        self.session_update_time = 0
        self.timeout = 60 * 9  # 9 minutes

    def get_session_token(self):
        elapsed = time.time() - self.session_update_time
        if elapsed > self.timeout:
            self.session_token = request_session_token(self.secret)
            self.session_update_time = time.time()
        return self.session_token

    def to_text(self, audio):
        token = self.get_session_token()
        return to_text(self.device_id, token, audio)


def request_session_token(secret):
    headers = {'Content-type': 'application/x-www-form-urlencoded',
               'Content-Length': "0",
               'Ocp-Apim-Subscription-Key': secret
               }
    r = requests.post(
        "https://api.cognitive.microsoft.com/sts/v1.0/issueToken", headers=headers)
    r.raise_for_status()
    return r.text


def to_text(device_id, token, audio):
    headers = {'Authorization': "Bearer " + token,
               'Content-type': 'audio/wav; codec="audio/pcm"; samplerate=16000'}
    request_id = uuid.uuid4()
    params = {
        "scenarios": "smd",
            "appid": app_id,
            "locale": "en-US",
            "device.os": "linux",
            "version": "3.0",
            "format": "json",
            "instanceid": device_id,
            "requestid": request_id,
            "result.profanitymarkup": "0"
    }
    r = requests.post("https://speech.platform.bing.com/recognize",
                      params=params, headers=headers, data=audio, stream=True)
    r.raise_for_status()
    # TODO: Error handling
    return r.json()
