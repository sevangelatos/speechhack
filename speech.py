#!/usr/bin/env python

import uuid
import subprocess
import sys
import recorder
from speechtotext import SpeechToText


def get_secret():
    with open("api_key.txt", "r") as f:
        secret = f.readlines()[0].strip()
        return secret

def say(phrase):
    synth = subprocess.Popen(["festival", "--tts"], stdin=subprocess.PIPE)
    synth.communicate(phrase)

def on_record_event(event):
    print event

def listen_reply(stt, recorder):
    recorder.start_on_sound()
    response = stt.to_text( recorder.audio_frames() )
    print response
    text = response['header']['name']
    print text
    recorder.pause()
    say(text)
    return text.lower().find("quit") == -1


def main(argv):
    if argv[-1] == "--test":
        import doctest
        errors, all_tests = doctest.testmod()
        sys.exit(errors)

    device_id = uuid.uuid4()
    rec = None
    try:
        stt = SpeechToText(get_secret(), device_id)
        rec = recorder.Recorder()
        while listen_reply(stt=stt, recorder=rec):
            pass
    except BaseException as ex:
        print ex
        say(ex.message)
    
    if rec:
        rec.close()


if __name__ == "__main__":
    main(sys.argv)
